﻿using Listview_Cells.Pages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Listview_Cells
{
    public class information //class function that collects data
        {
        public string name { get; set; }
        public string smallprint { get; set; }
        public string image { get; set; }
        
        }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class fruit : ContentPage
    {
        public ObservableCollection<information> list1 { get; set; } //makes an observable collection from the class of information

        public fruit()
        {
            InitializeComponent();
            cell_data(); //collects data from a function
            //was not used since wasnt able to use remove function with list
            // list.ItemsSource = new List<information>()
         }
        private void cell_data() //called to populate the observable collection
        {
            list1 = new ObservableCollection<information>() //viewcell created from observable collection
            {
                new information() //new viewcells created from information class
                {
                    name = "Red Apple" ,  smallprint = "2 lbs of apples can make a 9-inch pie" , image = "apple.png",
                },
                new information()
                {
                    name = "Banana" , smallprint = "Banana's can help lower blood pressure" , image = "ban.png"
                },
                new information()
                {
                    name = "BlueBerry" , smallprint = "Cup of blueberries contain 80 calories" , image = "blueberry.png"
                },
                new information()
                {
                    name = "Carrot" , smallprint = "No it does not improve your vision" , image = "carrot.png"
                },
                new information()
                {
                    name = "Green Apple" , smallprint = "Better than that red apple" , image = "gapple.png"
                },
                new information()
                {
                    name = "Grape" , smallprint = "Little round fruit that can be thrown at people" , image = "grape.png"
                },
                new information()
                {
                    name = "Kiwi" , smallprint = "Good but has weird skin" , image = "kiwi.png"
                },
                new information()
                {
                    name = "Mango" , smallprint = "Really sweet fruit" , image = "mango.png"
                },
                new information()
                {
                    name = "Orange" , smallprint = "Who likes orange juice ?" , image = "orange.png"
                },
                new information()
                {
                    name = "Pinapple" , smallprint = "If you like pina colada" , image = "pineapple.png"
                },
                new information()
                {
                    name = "Pomegranate" , smallprint = "It has a lot of seeds" , image = "pomegr.png"
                },
                new information()
                {
                    name = "Strawberry" , smallprint = "Has tiny seeds" , image = "strawberry.png"
                },
            };
            list.ItemsSource = list1; //makes the listview derived from list1 which is the source 
        }
        //this function will help define which cell will be clicked on by the user
        private async void list_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItemIndex == 0) 
            {
                await Navigation.PushAsync(new Page1());
            }
            else if (e.SelectedItemIndex == 1)
            {
                await Navigation.PushAsync(new Page2());
            }
            else if (e.SelectedItemIndex == 2)
            {
                await Navigation.PushAsync(new Page3());
            }
            else if (e.SelectedItemIndex == 3)
            {
                await Navigation.PushAsync(new Page4());
            }
            else if (e.SelectedItemIndex == 4)
            {
                await Navigation.PushAsync(new Page5());
            }
            else if (e.SelectedItemIndex == 5)
            {
                await Navigation.PushAsync(new Page6());
            }
            else if (e.SelectedItemIndex == 6)
            {
                await Navigation.PushAsync(new Page7());
            }
            else if (e.SelectedItemIndex == 7)
            {
                await Navigation.PushAsync(new Page8());
            }
            else if (e.SelectedItemIndex == 8)
            {
                await Navigation.PushAsync(new Page9());
            }
            else if (e.SelectedItemIndex == 9)
            {
                await Navigation.PushAsync(new Page10());
            }
            else if (e.SelectedItemIndex == 10)
            {
                await Navigation.PushAsync(new Page11());
            }
            else if (e.SelectedItemIndex == 11)
            {
                await Navigation.PushAsync(new Page12());
            }
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            var add = sender as MenuItem;
            var add_content = (information)add.CommandParameter;
            list1.Add(add_content); 
        }

        private void Delete_Clicked(object sender, EventArgs e)
        {
            var delete = sender as MenuItem; 
            var cell_delete = (information)delete.CommandParameter;
            list1.Remove(cell_delete);
        }

        private void list_Refreshing(object sender, EventArgs e)
        {
            cell_data();
            list.IsRefreshing = false;
        }
    }
}