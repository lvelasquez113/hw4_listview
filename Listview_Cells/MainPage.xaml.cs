﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Listview_Cells
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void food_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new fruit());
        }

        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(250); //gives a slight delay when appearing to the screen
            ContentPage content = mainpage; //allows the content page to be manipulated
            mainpage.BackgroundColor = Color.LightGray; //when appearing turns the screen to lightgray
            mainpage.BackgroundImageSource = "allfruit.png"; //changes it to fruit logo
        }

        async void mainpage_Disappearing(object sender, EventArgs e)
        {
            await Task.Delay(250); // gives a slight delay
            ContentPage content = mainpage; //allows the content page to be manipulated
            mainpage.BackgroundColor = Color.Crimson; //when appearing turns the screen to crimson
        }

    }
}
